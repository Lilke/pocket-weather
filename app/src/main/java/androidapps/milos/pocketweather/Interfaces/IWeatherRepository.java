package androidapps.milos.pocketweather.Interfaces;

import android.content.Context;

import java.io.Serializable;

import androidapps.milos.pocketweather.Models.WeatherModel;

/**
 * Created by Milos on 2/17/2017.
 */

public interface IWeatherRepository extends Serializable{
    void getWeatherDataByCityName(Context context, String cityName);
    void getWeatherDataByGeoLocation(String apiKey,double latitude,double longitude);
}
