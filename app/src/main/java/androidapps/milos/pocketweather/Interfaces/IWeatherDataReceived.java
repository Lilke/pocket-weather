package androidapps.milos.pocketweather.Interfaces;

import androidapps.milos.pocketweather.Models.WeatherModel;

/**
 * Created by Milos on 2/20/2017.
 */

public interface IWeatherDataReceived {

    void onWeatherDataReceived(WeatherModel weatherModel);
}
