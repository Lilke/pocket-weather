package androidapps.milos.pocketweather.Utilities;

/**
 * Created by Milos on 2/18/2017.
 */

public class ApixuRequestBuilder {
    private static String ApixuApiUrl = "http://api.apixu.com/v1/current.json";

    public static String CreateRequestForGettingDataByCityName(String apiKey,String cityName){
        return ApixuApiUrl + "?key=" + apiKey + "&"+ "q=" + cityName;
    }
}
