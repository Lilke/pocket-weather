package androidapps.milos.pocketweather.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidapps.milos.pocketweather.Interfaces.IWeatherRepository;
import androidapps.milos.pocketweather.R;
import androidapps.milos.pocketweather.Repositories.ApixuWeatherRepository;

public class WeatherMainActivity extends AppCompatActivity {
    static final String WeatherRepositoryApiKey = "aa916e0b53ad4f32b6e21911171502";
    Button btnGetWeatherDataByCityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_main);

        final WeatherMainActivity myMainActivity = this;

        referenceLayoutElements();


        //Here you can Change Your Repository To Desired One
        final IWeatherRepository weatherRepository = new ApixuWeatherRepository(WeatherRepositoryApiKey);

        btnGetWeatherDataByCityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog findCityPopupDialog = new Dialog(myMainActivity);
                findCityPopupDialog.setContentView(R.layout.modal_find_city_dialog);

                Button okBtn = (Button) findCityPopupDialog.findViewById(R.id.ok);
                okBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent weatherDetailsActivityIntent = new Intent(myMainActivity, WeatherDetailsActivity.class);
                        EditText etCityName = (EditText) findCityPopupDialog.findViewById(R.id.etCityName);
                        String cityName = etCityName.getText().toString();
                        weatherDetailsActivityIntent.putExtra("cityName", cityName);
                        weatherDetailsActivityIntent.putExtra("weatherDetails", weatherRepository);
                        startActivity(weatherDetailsActivityIntent);
                        findCityPopupDialog.dismiss();
                    }
                });
                Button cancelBtn = (Button) findCityPopupDialog.findViewById(R.id.cancel);
                cancelBtn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        findCityPopupDialog.dismiss();
                    }
                });

                findCityPopupDialog.show();
            }
        });


    }

    private void referenceLayoutElements() {
        btnGetWeatherDataByCityName = (Button) findViewById(R.id.btnWeatherCities);
    }

}
