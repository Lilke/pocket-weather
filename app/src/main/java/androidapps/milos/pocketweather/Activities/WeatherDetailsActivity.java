package androidapps.milos.pocketweather.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidapps.milos.pocketweather.Interfaces.IWeatherDataReceived;
import androidapps.milos.pocketweather.Interfaces.IWeatherRepository;
import androidapps.milos.pocketweather.Models.WeatherModel;
import androidapps.milos.pocketweather.R;

public class WeatherDetailsActivity extends AppCompatActivity implements IWeatherDataReceived {

    ProgressDialog progressDialog;
    TextView tvCityName,tvWeatherCondition,tvTemperature,tvFeelsLike,tvWindSpeed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);

        referenceLayoutElements();

        Intent weatherRepositoryIntent = getIntent();
        String cityName = weatherRepositoryIntent.getStringExtra("cityName");
        IWeatherRepository weatherRepository = (IWeatherRepository) weatherRepositoryIntent.getSerializableExtra("weatherDetails");

        weatherRepository.getWeatherDataByCityName(this,cityName);
    }




    @Override
    public void onWeatherDataReceived(WeatherModel weatherModel) {
        HideProgressDialog();

        UpdateWeatherData(weatherModel);
    }

    private void UpdateWeatherData(WeatherModel weatherModel) {


        if(weatherModel.weatherLocation == null){
            tvCityName.setText("City not Found!");
        }
        else{
            tvCityName.setText(weatherModel.weatherLocation.name);
            tvWeatherCondition.setText("Condition: " + weatherModel.currentWeather.weatherCondition.condition);
            tvTemperature.setText("Temperature: " + weatherModel.currentWeather.temp_c + " C");
            tvFeelsLike.setText("Feels: " + weatherModel.currentWeather.feelslike_c + " C");
            tvWindSpeed.setText("Wind Speed: " + weatherModel.currentWeather.wind_kph + " kph");
        }
    }

    private void referenceLayoutElements(){
        tvCityName  = (TextView) findViewById(R.id.tvCityName);
        tvWeatherCondition  = (TextView) findViewById(R.id.tvCondition);
        tvTemperature  = (TextView) findViewById(R.id.tvTemperature);
        tvFeelsLike = (TextView) findViewById(R.id.tvFeelsLike);
        tvWindSpeed = (TextView) findViewById(R.id.tvWindSpeed);
        ShowProgressDialog("Please wait","Downloading Weather Data...");
    }

    private void ShowProgressDialog(String dialogTitle,String dialogMesage) {
        progressDialog = ProgressDialog.show(this,dialogTitle,dialogMesage);
    }

    private void HideProgressDialog(){
        progressDialog.dismiss();
    }
}
