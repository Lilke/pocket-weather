package androidapps.milos.pocketweather.Models;

import android.graphics.Bitmap;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Milos on 2/17/2017.
 */

public class WeatherModel implements Serializable {

    @SerializedName("location")
    public WeatherLocation weatherLocation;

    @SerializedName("current")
    public CurrentWeather currentWeather;
}
