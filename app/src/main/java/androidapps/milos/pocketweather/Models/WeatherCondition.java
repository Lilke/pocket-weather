package androidapps.milos.pocketweather.Models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Milos on 2/17/2017.
 */

public class WeatherCondition implements Serializable{

    @SerializedName("text")
    public String condition;

    @SerializedName("icon")
    public String icon;

    @SerializedName("code")
    public int code;
}
