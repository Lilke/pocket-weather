package androidapps.milos.pocketweather.Models;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Milos on 2/17/2017.
 */

public class WeatherLocation implements Serializable {
    private static final long serialVersionUID = 1L;

    @SerializedName("name")
    public String name;

    @SerializedName("region")
    public String region;

    @SerializedName("country")
    public String country;

    @SerializedName("tz_id")
    public String tz_id;

    @SerializedName("localtime")
    public String localtime;

    @SerializedName("lat")
    public double lat;

    @SerializedName("lon")
    public double lon;

    @SerializedName("localtime_epoch")
    public int localtime_epoch;
}
