package androidapps.milos.pocketweather.Repositories;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import androidapps.milos.pocketweather.Interfaces.IWeatherDataReceived;
import androidapps.milos.pocketweather.Interfaces.IWeatherRepository;
import androidapps.milos.pocketweather.Models.WeatherModel;
import androidapps.milos.pocketweather.Utilities.ApixuRequestBuilder;

/**
 * Created by Milos on 2/17/2017.
 */

public class ApixuWeatherRepository implements IWeatherRepository {

    private String apiKey;


    public ApixuWeatherRepository(String apiKey){
        this.apiKey = apiKey;
    }


    @Override
    public void getWeatherDataByCityName(Context context,String cityName) {
        String currentWeatherRequest = ApixuRequestBuilder.CreateRequestForGettingDataByCityName(apiKey,cityName);

        getWeatherData getWeatherDataTask = new getWeatherData(context,currentWeatherRequest);
        getWeatherDataTask.execute();
    }

    @Override
    public void getWeatherDataByGeoLocation(String apiKey, double latitude, double longitude) {

    }

    public class getWeatherData extends AsyncTask<String,Void,WeatherModel>{

        private IWeatherDataReceived weatherDataReceivedCallback;
        private String currentWeatherRequest;

        public getWeatherData(Context context,String currentWeatherRequest ){
            this.weatherDataReceivedCallback = (IWeatherDataReceived) context;
            this.currentWeatherRequest = currentWeatherRequest;
        }

        @Override
        protected WeatherModel doInBackground(String... strings) {
            String currentWeatherData = downloadWeatherData(currentWeatherRequest);
            WeatherModel weatherModel = null;

            if(!TextUtils.isEmpty(currentWeatherData)){
                JSONObject weatherModelJson;
                try{
                    weatherModelJson = new JSONObject(currentWeatherData);
                    Gson gson=new GsonBuilder().create();
                    weatherModel = gson.fromJson(weatherModelJson.toString(),WeatherModel.class);


                    System.out.println("weatherModel==============>"+weatherModel);
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }

            return weatherModel;

        }

        @Override
        protected void onPostExecute(WeatherModel weatherModel) {
            super.onPostExecute(weatherModel);
                weatherDataReceivedCallback.onWeatherDataReceived(weatherModel);

        }


    }

    private String downloadWeatherData(String currentWeatherRequest){
        HttpURLConnection urlConnection = null;
        try{
            URL uri = new URL(currentWeatherRequest);

            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();


            if (statusCode != urlConnection.HTTP_OK) {
                return null;
            }


            InputStream weatherDataInputStream = new BufferedInputStream(urlConnection.getInputStream());

            if(weatherDataInputStream != null){
                BufferedReader reader=new BufferedReader(new InputStreamReader(weatherDataInputStream));
                StringBuilder stringBuilder =new StringBuilder();

                String line;
                while((line=reader.readLine())!=null)
                {
                    stringBuilder.append(line);
                }

                weatherDataInputStream.close();
                String weatherData=stringBuilder.toString();
                return  weatherData;

            }



        } catch (Exception e) {
            urlConnection.disconnect();
            Log.w("WeatherDataDownloader", "Error downloading weather data from" + currentWeatherRequest);
        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;

    }

    private Bitmap downloadBitmap(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode != urlConnection.HTTP_OK) {
                return null;
            }

            InputStream inputStream = urlConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        } catch (Exception e) {
            urlConnection.disconnect();
            Log.w("ImageDownloader", "Error downloading image from " + url);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
