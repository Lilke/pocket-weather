This is a simple Demo Android Application for getting Weather Data over API for searched City.

User simply opens App and click on "Find City Weather Location", dialog pops up, he enters city name, and for few moments he gets weather info for that city.

Application is still in development so it lacks many features(animations, images etc...)